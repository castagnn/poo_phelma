// Exemple illustrant static... Un bete compteur d'instance avec id automatique
//     La notion de membre de classe (static)
// Nicolas Castagné, 2017


// une classe un peu bête....
class CompteEnBanque {
    int numero;
    int solde;

    private static int nbInstances = 0; // un attribut de classe. On l'initialise lors de la déclaration, pas dans le constructeur !

    public CompteEnBanque(int solde) {
        this.solde = solde;
        CompteEnBanque.nbInstances ++; 	// ou 	nbInstances ++ ;
        this.numero = nbInstances;			// ou 	numero = nbInstances ;
    }

    @Override
    public String toString() {
        return "je suis un CompteEnBanque. numero=" + numero + ". solde=" + solde;
    }

    // une méthode de classe
    static int getNbInstances() {
        return nbInstances;
    }
}


public class TestStatic {

    public static void main(String[] args) {
				CompteEnBanque c1 = new CompteEnBanque(67);
				System.out.println("valeur : " + c1);

				CompteEnBanque c2 = new CompteEnBanque(7654);
				System.out.println("valeur : " + c2);

        //////////////////////////
        // Exemple d'appel d'appel d'une méthode static
        int nbAlloues = CompteEnBanque.getNbInstances(); // méthode static => on peut utiliser le nom de la classe
        System.out.println("Au total, " + nbAlloues + " objets compte ont été alloués depuis le lancement du programme");


        //////////////////////////////
        // 2. tableau de comptes
        int n = 6;
        // declaration et allocation d'un tableau de références sur des objets de type CompteEnBanque
        CompteEnBanque[] tabDeComptes = new CompteEnBanque[n];
        // => les références sont initialisées à null

        // On crée des objets et on stocke des références dans le tableau
        for (int i = 0; i < tabDeComptes.length; i++) {
            tabDeComptes[i] = new CompteEnBanque(25*i);  // allocation objet par objet
        }
        // Quizz :  pourquoi ne peut on pas utiliser une boucle foreach ci dessus ?

        // On affiche les objets CompteEnBanque

        System.out.println("Affichage du contenu du tableau tabDeComptes");
        for (CompteEnBanque unCompte : tabDeComptes) {
            System.out.println("valeur : " + unCompte); // rappel : exécute unCompte.toString()
        }


        //////////////////////////
        // Exemple d'appel d'appel d'une méthode static
        System.out.println("Au total, " + CompteEnBanque.getNbInstances() + " objets compte ont été alloués depuis le lancement du programme");
    }
}
