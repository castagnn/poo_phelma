// Exemple illustrant :
//		Les tableaux en Java
//      La notion de membre de classe (static)
// Nicolas Castagné, 2017

import java.util.Arrays;
import java.util.Random;

// une classe un peu bête....
class CompteEnBanque {
    int solde;
    
    public CompteEnBanque(int solde) {
        this.solde = solde;
    }
    
    @Override
    public String toString() {
        return "je suis un CompteEnBanque. solde=" + solde;
    }
}


public class TestTableaux {
    
    public static void main(String[] args) {
        //////////////////////////////
        // 1. Un premier tableau d'entiers
        int[] tab;         // déclaration, tab n'est pas encore initalisé.
        // tab est une référence, initialisée à null
        
        tab = new int[5];  // allocation du tableau;
        // => les indices vont de 0 à tab.length - 1, ici 4
        // => les valeurs sont initialisées à 0
        // => la taille du tableau ne peut plus être modifiée après
        
        // accès via l'operateur []
        tab[0] = 2;
        System.out.println("valeur à l'indice 4 -> " +  tab[4] ); // affiche valeur à l'indice 4 -> 0
        
        // Que se passe-t-il ?
        tab[5] = 45 ; 	// =>	ArrayIndexOutOfBoundsException
        
        // Parcours par une boucle
        // et utilisation de l'attribut  ".length", qui est immutable (déclaré "final")
        for (int i = 0; i < tab.length; i++) {
            tab[i] = 2*i;
        }
        
        // Autre parcours, de type "for each"
        // (pour appliquer un traitement à *tous* les éléments contenu dans le tableau)
        // Lire "pour chaque entier "valeur" dans le tableau tab..."
        for (int valeur : tab) {
            System.out.println("valeur : " + valeur);
        }
        
        
        //////////////////////////////
        // 2. tableau 1D d'objets (ou plutôt : de références)
        int n = 6;
        // declaration et allocation d'un tableau de références sur des objets de type CompteEnBanque
        CompteEnBanque[] tabDeComptes = new CompteEnBanque[n];
        // => les références sont initialisées à null
        
        // Affiche n fois "null"
        for (CompteEnBanque unCompte : tabDeComptes) {
            System.out.println("valeur : " + unCompte);
        }
        
        // On crée des objets et on stocke des références dans le tableau
        for (int i = 0; i < tabDeComptes.length; i++) {
            tabDeComptes[i] = new CompteEnBanque(2*i);  // allocation objet par objet
        }
        // Quizz :  pourquoi ne peut on pas utiliser une boucle foreach ci dessus ?
        
        // On affiche les objets CompteEnBanque
        
        System.out.println("Affichage du contenu du tableau tabDeComptes");
        for (CompteEnBanque unCompte : tabDeComptes) {
            System.out.println("valeur : " + unCompte); // rappel : exécute unCompte.toString()
        }
        
        // Comprenons bien ce qui se passe en mémoire...
        CompteEnBanque[] tabDeComptes2 = tabDeComptes; 	//que se passe-t-il en mémoire ?
        tabDeComptes2[0] = new CompteEnBanque(1111); 	//que se passe-t-il en mémoire ?
        
        // que se passe-t-il en mémoire ?
        tabDeComptes2 = new CompteEnBanque[n];
        for (int i = 0; i < tabDeComptes2.length; i++) {
            tabDeComptes2[i] = tabDeComptes[i];
        }
        
        //////////////////////////////
        // 3. Tableau 2D
        int[][] tab2D;
        tab2D = new int[5][]; // tab de 5 références null
        for (int i = 0; i < tab2D.length; i++) {
            // allocation case par case. Dimensions par forcément identiques
            tab2D[i] = new int[i + 1];
        }
        
        // et maintenant, tout simplement :
        tab2D[1][0] = 45;
        
        
        //////////////////////////////
        // 4. Matrice : tableau 2D où toutes les lignes ont la même taille
        // peut être créé d'un seul coup par :
        int [][] matrice = new int [5][10];
        // une matrice de 5 lignes et 10 colonnes contenant des 0
        matrice[4][6] = 8;
        
        
        //////////////////////////////
        // 5. Examples d'usage de la classe Arrays (=> notion secondaire pour ce module)
        int[] vec = new int[5];
        Random rn = new Random(); // pour avoir un générateur de nombre aléatoire
        for (int i = 0; i < tab.length; i++) {
            vec[i] = rn.nextInt(1000); // nb aleatoire entre 0 et 999
        }
        
        // tri : Utilisation de la méthode de classe sort() de la classe Arrays
        // qui implémente l'algorithme "quicksort" => en O(n*log(n)))
        Arrays.sort(vec);
        for (int valeur : vec) {
            System.out.println("valeur triées : " + valeur);
        }
        
        // Recherche, avec la méthode de classe binarySearch() de la classe Arrays
        // Il faut que le tableau soit trié avant la recherche...
        int pos = Arrays.binarySearch(vec, 500);
        if( pos >= 0 )
            System.out.println("position de 500 : " + pos);
        else
            System.out.println("500 n ’est pas dans le tableau");
        
        
        // Copie, avec la méthode de classe copyOf() de la classe Arrays
        int[] vec2 = Arrays.copyOf(vec, vec.length);
        vec2[3] = 5;
        // Que se passe-t-il en mémoire ?
        // Que se passe-t-il si on copie, avec Arrays.copyOf() un tableau "contenant" des objets ?
    }
}
